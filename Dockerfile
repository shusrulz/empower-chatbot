FROM python:3.6
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
RUN apt install -y supervisor
RUN apt-get update && apt-get install -y nodejs
RUN apt-get update && apt-get install -y cmdtest
RUN apt-get update && apt-get install -y yarn
RUN apt-get update && apt-get install -y npm
RUN npm install npm@latest -g
RUN npm install http-server -g
ADD . /app
WORKDIR /app
RUN pip install rasa
RUN pip3 install spacy
RUN python3 -m spacy download en_core_web_md
RUN python3 -m spacy link en_core_web_md en
RUN rasa train
CMD rasa run actions --port 5056
CMD rasa run --cors "*" --port 5006
CMD http-server build -p 5025
COPY ./init/supervisord.conf /etc/supervisor/conf.d/
CMD ["/usr/bin/supervisord"]

