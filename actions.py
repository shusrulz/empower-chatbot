import csv
import random

from pymongo import MongoClient
from rasa_core_sdk.executor import CollectingDispatcher
from rasa_core_sdk.events import UserUtteranceReverted
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet

from myTracker import insert_logs

client = MongoClient()
client = MongoClient('mongodb://localhost:27017/')
db = client['EMPOWER_LOGS']
collection = db['empowercollection']


class ActionShowTypeOfAccount(Action):
    def name(self):
        return 'actionshowtypeofaccount'

        buttons = [{'title': 'Loan',
                   'payload': '{}'.format('loan')},
                  {'title': 'Deposit',
                   'payload': '{}'.format("deposit")}   
                   ]
        user_id =tracker.sender_id

        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        try:
            insert_logs(user_id,query,message,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_button_message(message, buttons=buttons)
        return []

class FetchTypeOfAccount(Action):
    def name(self):
        return 'fetchtypeofaccount'

    def run(self, dispatcher, tracker, domain) -> List[Dict[Text, Any]]:
        account_type = tracker.get_slot('account_type')
        if (account_type == 'Loan' or account_type == 'loan'):
            response = "Oh. You want to change interest rate for Loan. Please refer to Loan Interest Rate Change Maintenance 12267. You can follow this link <a href = 'http://139.5.71.109:8090/BANKING/Loan/Loan_IntRate_Change/LoanIntRateChange.aspx' target='_blank'>Change Loan Rate</a>"
        else:
            response = "Oh. You want to change interest rate for Deposit. Please refer to Deposit Interest Rate Change Maintenance 12266. Follow the link here <a href = 'http://139.5.71.109:8090/BANKING/Deposit/Deposit_Int_Rate_Change/DepositIntRateChange.aspx' target='_blank'>Change Deposit Rate</a>"

        user_id =tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        try:
            insert_logs(user_id,query,response,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass

        dispatcher.utter_message(response)

        return []

class OutOfScope(Action):
    def name(self) -> Text:
        return "action_out_of_scope"

    def run(self,dispatcher: CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any],)->List[Dict[Text,Any]]:
        reply = "Sorry I do not think I can handle your query. Please use the help from our support team. email: support@infodevelopers.com.np landline: +977-1-5542355, 5541757, 5546470, 5013196, 5013166, 5013197, 5546471 support cell: 9840029557, 9840029558, 9840029559, 9840029560, 9840029561, 9840029562, 9840029563, 9840029563, 9840029565, 9840029556 (10am to 5:30pm only)"
        dispatcher.utter_message(reply)
        return [UserUtteranceReverted()]

class ActionStoreLogs(Action):
    def name(self) -> Text:
        return "actionstorelogs"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:

        with open('support_logs.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["User_ID", "Query", "Intent", "Intent Confidence", "Entilties", "Reply","Timestamp"])
            for document in collection.find():
                user_id = document['user_id']
                query = document['query']
                intent = document['intent']['name']
                intent_confidence = document['intent']['confidence']
                entities = document['entities']
                if len(entities)!=0:
                    entity_list = []
                    for ent in entities:
                        entity_list.append(ent['value'])
                else:
                    entity_list = None
                reply = document['reply']
                timestamp = document['timestamp']
                writer.writerow([user_id, query,intent, intent_confidence,entity_list,reply,timestamp])
        dispatcher.utter_message("Okay, I will store these chat logs with you.")
        return []

class AboutInfinity(Action):
    def name(self) -> Text:
        return "action_about_INIFINITY_bot"

    def run(self,dispatcher: CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any],)->List[Dict[Text,Any]]:
        reply = ["This chatbot is developed as for the inhouse product INIFINITY of InfoDevelopers Pvt Ltd. This chatbot aims to aid general issue handling in the support department of the company. This chatbot can help in solving general and simple issues.",
                "INFINITY chatbot is the conversational agent developed as the inhouse product INIFINITY of InfoDevelopers Pvt Ltd. This chatbot aims to aid general issue handling in the support department of the company. This chatbot can help in solving general and simple issues.",
                "This is INFINITY chatbot.It is a inhouse product INIFINITY of InfoDevelopers Pvt Ltd. This chatbot aims to aid general issue handling in the support department of the company. This chatbot can help in solving general and simple issues."]

        user_id =tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass

        dispatcher.utter_message(message)
        return []

class LoanScheduleCalculator(Action):
    def name(self) -> Text:
        return "action_about_loan_schedule_calculator"

    def run(self,dispatcher: CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any],)->List[Dict[Text,Any]]:
        reply = ["Loan schedule calculator is used to view/calculate the schedule of loan products and how its schedule would be if an account was created in similar condition."]

        user_id =tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass

        dispatcher.utter_message(message)
        return []

class ObligatorCustomerInformation(Action):
    def name(self) -> Text:
        return "action_about_obligator_customer_information"

    def run(self,dispatcher: CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any],)->List[Dict[Text,Any]]:
        reply = ["Obligator is similar to customer group."]

        user_id =tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass

        dispatcher.utter_message(message)
        return []

class AboutOverdraftLoan(Action):
    def name(self) -> Text:
        return "action_about_overdraft_loan"

    def run(self,dispatcher: CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any],)->List[Dict[Text,Any]]:
        reply = ["Overdraft loan or od loan refers to loan where the loan limit decreases when payment is done."]

        user_id =tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass

        dispatcher.utter_message(message)
        return []

class AboutScheduleLoanAccount(Action):
    def name(self) -> Text:
        return "action_about_schedule_loan_account"

    def run(self,dispatcher: CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any],)->List[Dict[Text,Any]]:
        reply = ["Oh you want to know about schedule loan amount. The schedule of a loan account is created during the disbursal process because during deposit ac opening the disburse limit is not defined and creation of schedule during opening would cause inaccurate schedule if multiple disbursement is done."]

        user_id =tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass

        dispatcher.utter_message(message)
        return []

class AboutZeroPrincipal(Action):
    def name(self) -> Text:
        return "action_about_zero_principal"

    def run(self,dispatcher: CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any],)->List[Dict[Text,Any]]:
        reply = ["Zero principal loan settlement screen is used to settle the loan account which doesn’t have any principal balance but still has outstanding interest balance."]

        user_id =tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent = intent,entities = entities)
            print("inserted")
        except Exception as error:
            print('Caught this error: ' + repr(error))
            pass

        dispatcher.utter_message(message)
        return []

class AddRights(Action):
    def name(self) -> Text:
        return "action_add_rights"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["User rights can set through User Group Menu Rights Maintenance -11044 by select user group, menu type and select menu for more right and provide privilege too. Follow this link <a href = 'http://139.5.71.109:8090/Public/Emp_UserGroupMenuRightsMain.aspx' target='_blank'>User Right</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class AfterGreet(Action):
    def name(self) -> Text:
        return "action_after_greet"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["I am doing very well. You have any questions regarding features of Infinity that you want help with?",
                "I am doing perfectly fine. If you have any queries reagarding Infinity services, ask me.",
                "I am feeling very great today. You want to know about Infinity? Ask me if you want any help with it."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class BranchEnrollmentEmployee(Action):
    def name(self) -> Text:
        return "action_branch_enrollment_of_employee"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Refer to 12222 – Employee Office Assignment Report under Banking – Banking Report. You can follow this link <a href = 'http://139.5.71.109:8090/BANKING/Banking_Report/EmployeeOfficeAssignmentReport.aspx' target='_blank'>Employee Office Assignment Report</a>",
                "For the information of enrollment of any employee in branch, you can refer to the section 12222 – Employee Office Assignment Report under Banking – Banking Report. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Banking_Report/EmployeeOfficeAssignmentReport.aspx' target='_blank'>Employee Office Assignment Report</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ChangeCustomerHolder(Action):
    def name(self) -> Text:
        return "action_change_customer_holder"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can change the customer holder deposit account through a/c holder maintenance"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ChangeInterestAmount(Action):
    def name(self) -> Text:
        return "action_change_interest_amount"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please navigate to the Deposit/loan interest adjustment Maintenance section"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ChangeRecurringInstallmentAmount(Action):
    def name(self) -> Text:
        return "action_change_recurring_installment_amount"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can change the recurring installment amount from the Recurring Installment Change Maintenance section."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class CheckDailyTransaction(Action):
    def name(self) -> Text:
        return "action_check_daily_transaction"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Check your daily transaction through Daily Transaction Report-11082. Follow this link <a href = 'http://139.5.71.109:8090/BANKING/Account_Report/Account_DailyTransaction.aspx' target='_blank'>Account Daily Transction</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class CheckUserGroupRight(Action):
    def name(self) -> Text:
        return "action_check_user_group_right"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can check rights of user group from Security Report-11360. Go through this link <a href = 'http://139.5.71.109:8090/BANKING/Account_Report/Security/SecurityReportView.aspx' target='_blank'>Security Report View</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ChequeStatusReport(Action):
    def name(self) -> Text:
        return "action_cheque_status_report"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["From Cheque Status Maintenance -01009  you can check  cheque status report. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Instrument_Setup/CheckStatusMaintenance.aspx?id=209' target='_blank'>Check Status Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class CreateBankAccount(Action):
    def name(self) -> Text:
        return "action_create_bank_account"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You need to follow below steps for creating bank account:👇 1.Bank Information Maintenance - 01011 (Bank Information). Follow this link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Accounts/Emp_BankInformationMain.aspx' target='_blank'>Employee Bank Information</a> 2.A/C Information Maintenance - 11367 (Bank a/c Information). Follow this link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Accounts/Emp_BankMaintenance.aspx' target='_blank'>Employee Bank Maintenance</a> 3.Chart of Account - 01003 (Bank ledger in system). Follow this link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Accounts/CharOfAccount/ChartofAccount.aspx' target='_blank'>Chart of Account</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []


class CreateCollector(Action):
    def name(self) -> Text:
        return "action_create_collector"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Visit collector information maintenance(11062) and fill collector information. Follow this link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Information/Emp_CollectorInforMain.aspx' target='_blank'>Employee Collector Information</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class CreateEmployeeAccount(Action):
    def name(self) -> Text:
        return "action_create_employee_account"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can create employee account Only in those product which is open by Account Group Parameter Maintenance - 01002 with staff product. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Product/Emp_ACGroupParameterMaintenance.aspx' target='_blank'>Employee Account Group Parameter Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class CreateGeneralLedger(Action):
    def name(self) -> Text:
        return "action_create_general_ledger"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can create general ledger through Chart of Account-01003. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Accounts/CharOfAccount/ChartofAccount.aspx' target='_blank'>Chart of Account</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class CreateNewCustomer(Action):
    def name(self) -> Text:
        return "action_create_new_customer"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Interest rates of deposit and loan accounts can be changed through deposit interest rate change and loan interest rate change maintenance."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class CustomerLoanBalance(Action):
    def name(self) -> Text:
        return "action_customer_loan_balance"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can find customer loan balance from Customer All Loan Balance Report - 11462. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Loan/loan_Report/CustomerAllLoanBalanceReportNew.aspx' target='_blank'>Customer Loan Balance Report</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class DayEndTooLong(Action):
    def name(self) -> Text:
        return "action_day_end_too_long_to_complete"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Check Day-end Status Report - 12345 under General - General Report. If the process has failed, contact the vendor. You can follow the given link <a href = 'http://139.5.71.109:8090/BANKING/General/DayEndStatusReport/DayEndStatusReport.aspx' target='_blank'>Day End Status Report</a>",
                "If the Day End is not completed, you can check Day-end Status Report - 12345 under General - General Report. If the process has failed, contact the vendor. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/General/DayEndStatusReport/DayEndStatusReport.aspx' target='_blank'>Day End Status Report</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class DecreaseLoanLimit(Action):
    def name(self) -> Text:
        return "action_decrease_loan_limit"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please chek Limit Sanction Maintenance section."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class DenominationVault(Action):
    def name(self) -> Text:
        return "action_denomination_vault"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can find details of denimination vault on Vault Denomination Report-11192. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Account_Report/VautDenomination/VaultDenominationReport.aspx' target='_blank'>Vault Denomination Report</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class DepositRenew(Action):
    def name(self) -> Text:
        return "action_deposit_renew"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please check IS_DEPOSIT_AC_RENEW_IN_SAME_AC_NO in Global Parameter Setup-11111. Follow the given link <a href = 'http://139.5.71.109:8090/PUBLIC/GlobalParameterSetUp.aspx' target='_blank'>Global Parameter Setup</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class EmployeeDisplaySalarySheet(Action):
    def name(self) -> Text:
        return "action_employee_display_salary_sheet"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You need to assign employee to branch from screen Branch Employee Maintenance"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ExpiredLoanReport(Action):
    def name(self) -> Text:
        return "action_expired_loan_report"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can see loan expiry report from Loan Transaction Report- 11273. Follow the provided link <a href = 'http://139.5.71.109:8090/BANKING/Loan_Report/LoanTransaction/LoanTransactionReport.aspx' target='_blank'>Loan Transaction Report</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class FillNonResidentialInformation(Action):
    def name(self) -> Text:
        return "action_fill_non_residential_information"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Click 'YES' in is none resident of customer information maintenance and fill up the Non Resident details"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class FillOtherBankInformation(Action):
    def name(self) -> Text:
        return "action_fill_other_bank_information"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Click 'YES' in dealing with other bank of customer information and fill up other bank details in other information"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class FindCustomerInformation(Action):
    def name(self) -> Text:
        return "action_find_customer_information"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["To find customer information, please visit Customer Liability Report-11105. You can follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Banking_Report/Banking_CustomerLiabilityNew.aspx' target='_blank'>Banking Customer Liability</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class FindUserList(Action):
    def name(self) -> Text:
        return "action_find_user_list"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Navigate to User List Report - 12195 to find all the users. You can follow this link <a href = 'http://139.5.71.109:8090/GENRAL/UserLists.aspx' target='_blank'>User List</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class GlStatement(Action):
    def name(self) -> Text:
        return "action_gl_statmenet"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can see GL statement by navigating to GL Statement Report Maintenance-00001. Click on the provided link <a href = 'http://139.5.71.109:8090/BANKING/Account_Report/BankingGlStatement/BankingGlStatementMain.aspx' target='_blank'>Banking GL Satement</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class Goodbye(Action):
    def name(self) -> Text:
        return "action_goodbye"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["👋 Thank you for using INIFINITY chatbot.",
                "👋 I am very happy you used this chatbot. You are always welcome here."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class Greeting(Action):
    def name(self) -> Text:
        return "action_greeting"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Hello 👋, this is INIFINITY chatbot. We can assist you in your problems faced in this system. How can I assit you.",
                "Welcome 🙏 to INFINITY chatbot. We can assist you in your problems faced in the INFINITY system. How can I help you",
                "Namaste 🙏. This is INFINITY chatbot. This chatbot can help you to solve problems in the INFINITY system. How may I come to your service."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class InstallmentScheduleNoneRecurringProduct(Action):
    def name(self) -> Text:
        return "action_installment_schedule_none_recurring_product"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Follow user installment schedule from None Recurring Installment Amount-11537. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Account/Emp_NoneRecurringInstallmentAmount.aspx' target='_blank'>Employee None Recurring Installment Amount</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class InterBranchTransaction(Action):
    def name(self) -> Text:
        return "action_inter_branch_transaction"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Here is something i can tell you about inter bank transaction. Inter branch transaction refers to the balance transfer among different branches using remit GL."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class LoanDisbursementReverse(Action):
    def name(self) -> Text:
        return "action_loan_disbursement_reverse"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Yes, it is able through Loan Disbursement Reversal Maintenance-11455 but only of same date only. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Loan/Emp_LoanDisbursementReversalMain.aspx' target='_blank'>Loan Disbursement Reversal</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class MandotryFieldKyc(Action):
    def name(self) -> Text:
        return "action_mandotry_field_kyc"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Go to Customer Mandatory Information Maintenance - 12254. Click new and tick the required field and save it. You can follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Information/CustomerInformationValidationOptionSetUp/CustomerInformationValidationOptionSetUp.aspx' target='_blank'>Customer Information Validation Option</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class MenuUnavailableAfterLogging(Action):
    def name(self) -> Text:
        return "action_menu_unavailable_after_logging"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["In the URL, remove any kinds of clutter/cookies. The URL of the login page should only be https://secure.bittiyasewa.com/",
                "If you are facing such problems, you should remove any kinds of clutter/cookies in the URL.The URL of the login page should only be https://secure.bittiyasewa.com/",
                "Oh your menu is not displaying, we suggest you to remove any kinds of clutter/cookies. The URL of the login page should only be https://secure.bittiyasewa.com/"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class NepaliInformationKyc(Action):
    def name(self) -> Text:
        return "action_nepali_information_in_kyc"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Go to Customer Nepali Information Maintenance-12070. Keep the customer code and fill up the customer information in Nepali. You can follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Information/CustomerNepaliInformation.aspx' target='_blank'>Customer Nepali Information</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class NotDisplayingPageForm(Action):
    def name(self) -> Text:
        return "action_not_displaying_page_form"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Use the latest version of Firefox and force refresh (ctrl+f5) the page where you are facing the problem.",
                "If you are facing such problems, use the latest version of Firefox and force refresh (ctrl+f5) the page where you are facing the problem."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class NotGeneratingLoanOrDepositSchedule(Action):
    def name(self) -> Text:
        return "action_not_generating_loan_or_deposit_schedule"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Check if center meeting has been generated or, in case of center meeting frequency change, check if the meeting has been approved",
                "If the schedule is not generated, you can check if center meeting has been generated or, in case of center meeting frequency change, check if the meeting has been approved.",
                "For this problem you can Check if center meeting has been generated or, in case of center meeting frequency change, check if the meeting has been approved"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class OpenMultipleAccounts(Action):
    def name(self) -> Text:
        return "action_open_multiple_accounts"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Auto Account Opening can be done through Deposit Auto Account Opening -12342.You can also open multiple account in multiple setup. You can follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Deposit/DepositAutoAcOpening/DepositAutoAcOpening.aspx' target='_blank'>Deposit Auto Account Opening</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class PrintTrialBalance(Action):
    def name(self) -> Text:
        return "action_print_trial_balance"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Trail balance in infinity can be printed through default periodic report screen."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ProblemWithCustomerId(Action):
    def name(self) -> Text:
        return "action_problem_with_customer_id"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Make sure that the type of the customer is Employee (Can be checked from Customer Information Maintenance - 01024. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Information/Emp_CustomerInfoMain.aspx' target='_blank'>Employee Customer Information</a>) and type of the deposit/ loan product is Staff Product (Can be checked from Account Group Parameter Maintenance - 01002. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Product/Emp_ACGroupParameterMaintenance.aspx' target='_blank'>Employee Account Group Parameter Maintenance</a>)",
                "If there is problem with the customer id, Make sure that the type of the customer is Employee (Can be checked from Customer Information Maintenance - 01024. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Information/Emp_CustomerInfoMain.aspx' target='_blank'>Employee Customer Information</a>) and type of the deposit/ loan product is Staff Product (Can be checked from Account Group Parameter Maintenance - 01002. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Product/Emp_ACGroupParameterMaintenance.aspx' target='_blank'>Employee Account Group Parameter Maintenance</a>)"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ProfaneWords(Action):
    def name(self) -> Text:
        return "action_profane_words"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["🤬 Such words are not accepted.",
                "🤬. We cannot accept such words from you.",
                "🤬 Please do not offend me."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ProvideSalary(Action):
    def name(self) -> Text:
        return "action_provide_salary"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You need to follow below steps for providing salary:👇 1. Employee Salary Head Maintenance - 11394. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Accounts/Emp_EmployeeSalaryHeadMaintenance.aspx' target='_blank'>Employee Salary Head Maintenance</a> 2. Employee Salary Distribution Maintenance-11384. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Account/Emp_EmployeeSalaryDistributionMain.aspx' target='_blank'>Employee Salary Distribution</a> 3. Global Parameter Setup-11111 (make CENTRALIZE_PAYROLL yes and update). Follow the given link <a href = 'http://139.5.71.109:8090/PUBLIC/GlobalParameterSetUp.aspx'target='_blank'>Global Paramter Setup</a> 4. Employee Batch Payment Transaction -12338. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Account/EmployeeBatchPayment/EmployeeBatchPayment.aspx' target='_blank'>Employee Batch Payment</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ReleaseAccountDormantStatus(Action):
    def name(self) -> Text:
        return "action_release_account_dormant_status"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can release a dormant account through dormant account release maintenance."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class RenewLoan(Action):
    def name(self) -> Text:
        return "action_renew_loan"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please check IS_LOAN_AC_RENEW_IN_SAME_AC_NO in Global Parameter Setup-11111. Go through this link <a href = 'http://139.5.71.109:8090/PUBLIC/GlobalParameterSetUp.aspx' target='_blank'>Global Paramter Setup</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ReverseDepositAccount(Action):
    def name(self) -> Text:
        return "action_reverse_deposit_account"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Yes, it is possible through Deposit A/C Closing Maintenance-02104 in same date only. Go through this link <a href = 'http://139.5.71.109:8090/BANKING/Deposit/Emp_DepositAcClosingMainTenance.aspx' target='_blank'>Employee Deposit Account Closing Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class RevertLoanPaymentVoucher(Action):
    def name(self) -> Text:
        return "action_revert_loan_payment_voucher"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["From Loan Payment Reversal Maintenance - 11123 you can revert loan payment voucher a)Same date through cash, a/c any. b) Previous date through a/c only. You can follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Loan/emp_loanpaymentreversalmaintenance.aspx' target='_blank'>Employee Loan Payment Reversal Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SeeBranchFromHeadOffice(Action):
    def name(self) -> Text:
        return "action_see_brach_from_head_office"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["To see branches from head office please follow Bank Branch A/C Report-11363. You can go through this link <a href = 'http://139.5.71.109:8090/BANKING/ACCOUNT_REPORT/ACCOUNT_BRANCHBANKACNEW.ASPX' target='_blank'>Branch Bank</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SeeStaffAdvance(Action):
    def name(self) -> Text:
        return "action_see_staff_advance"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["To view staff advance amount with respect to employee please follow Employee Balance Report-11388."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SeeSubLedgerBalanceTransaction(Action):
    def name(self) -> Text:
        return "action_see_sub_ledger_balance_and_transaction"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can view the sub ledge balance and transaction from Sub Ledger Balance Report-11387 and Sub ledger Transaction Report - 12321"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SeeVoucherDetails(Action):
    def name(self) -> Text:
        return "action_see_voucher_details"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Voucher details can be obtained from Voucher Query Maintenance-00002. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Account_Report/Account_VoucherQueryMaintenance.aspx' target='_blank'>Voucher Query Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SetHoliday(Action):
    def name(self) -> Text:
        return "action_set_holiday"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can set holiday from Calendar Maintenance -11072. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Product/EMP_CalendarMaintenance.aspx' target='_blank'>Calender Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SetLimitOnUser(Action):
    def name(self) -> Text:
        return "action_set_limit_on_user"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Go to screen User Group A/C Group Limit Maintenance - 01021. Select the user group and tick the ac group you want to set limit and double click it and set limit. Follow the given link <a href = 'http://139.5.71.109:8090/Public/Emp_UserGroupACGroupLimitMaintenance.aspx' target='_blank'>User Group Account Limit Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SetMessageAboutCustomer(Action):
    def name(self) -> Text:
        return "action_set_message_about_customer"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can set message about customer from Message Pinning-12301. You can follow this link <a href = 'http://139.5.71.109:8090/GENRAL/MessagePinning/MessagePinning.aspx' target='_blank'>Message Pinning</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SetRightsToUser(Action):
    def name(self) -> Text:
        return "action_set_rights_to_user"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please select the user from User Right Assignment Maintenance-11031 select the branch in which user has to be assign right and the user group save and approve it. You can follow the given link <a href = 'http://139.5.71.109:8090/Public/Emp_UserGroupAssignmentMain.aspx' target='_blank'>Employee User Group Assignment</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SetTellerRight(Action):
    def name(self) -> Text:
        return "action_set_teller_right"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["To set teller right for user navigate to Teller Setup Maintenance-11022. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Accounts/Emp_TellerSetupMaintenance.aspx' target='_blank'>Teller Setup Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SetTellerRightBranch(Action):
    def name(self) -> Text:
        return "action_set_teller_right_in_branch"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You have to login branch 002 first then go to Teller Setup Maintenance-11022 and assign the teller. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Accounts/Emp_TellerSetupMaintenance.aspx' target='_blank'>Teller Setup Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class SetupDormant(Action):
    def name(self) -> Text:
        return "action_setup_dormant"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Go to Global Parameter Setup-11111. Search KYC_UPDATE_PERIOD_IN_DAYS and fill up the KYC update date. Follow the given link <a href = 'http://139.5.71.109:8090/PUBLIC/GlobalParameterSetUp.aspx' target='_blank'>Global Paramter Setup</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ShowCustomerTransferReport(Action):
    def name(self) -> Text:
        return "action_show_customer_transfer_report"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Go to Customer Transfer Report-11763. Define the time period and print it. You can follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Banking_Report/Banking_CustomerTransferNew.aspx' target='_blank'>Banking Customer Transfer Report</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class ShowKycCopomisReport(Action):
    def name(self) -> Text:
        return "action_show_kyc_copomis_report"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Go to Customer Information Report - 12194. Define the time period and click on show box. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Banking_Report/Emp_CustomerInformationReport.aspx' target='_blank'>Customer Information Report</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class TagEmployeeSalarySheet(Action):
    def name(self) -> Text:
        return "action_tag_employee_salary_to_salary_sheet"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please navigate to the section: Employee information Maintenance"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class TalkToPerson(Action):
    def name(self) -> Text:
        return "action_talk_to_person"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["To talk with our support team use these details:  email: support@infodevelopers.com.np landline: +977-1-5542355, 5541757, 5546470, 5013196, 5013166, 5013197, 5546471 support cell: 9840029557 , 9840029558, 9840029559, 9840029560, 9840029561, 9840029562, 9840029563, 9840029563, 9840029565, 9840029556 (10am to 5:30pm only)"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class TellerBalanceStatus(Action):
    def name(self) -> Text:
        return "action_teller_balance_status"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please follow Teller Balance Status Report-11193 for teller balance status. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/ACCOUNT_REPORT/TELLERBALSTATUS/TELLERBALSTATUSREPORT.ASPX' target='_blank'>Teller Balance Status</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class TrailBalanceMismatch(Action):
    def name(self) -> Text:
        return "action_trail_balance_mismatch"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Check for missing GL mappings in 11075 - Periodic Report Group Mapping Maintenance under Account – Account Setup – Other Report Setup. If problem persists even after complete mapping, please contact software support. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Report/Emp_PeriodicReportGroupMappingMaintenance.aspx' target='_blank'>Periodic Report Group Mapping Maintenance</a>",
                "For this error you can check the missing GL mappings in 11075 - Periodic Report Group Mapping Maintenance under Account – Account Setup – Other Report Setup. If problem persists even after complete mapping, please contact software support. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Report/Emp_PeriodicReportGroupMappingMaintenance.aspx' target='_blank'>Periodic Report Group Mapping Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class TransferAmountBetweenAccounts(Action):
    def name(self) -> Text:
        return "action_transfer_amount_between_accounts"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please follow Standing Instruction Setup - 12312  to transfer amount between accounts. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Banking_Transaction/StandingInstruciton/StandingInstructionSetup.aspx' target='_blank'>Standing Instruction Setup</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class TransferCooperativeBalance(Action):
    def name(self) -> Text:
        return "action_transfer_cooperative_balance"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You can transfer the bank balance to vault through vault transaction maintenance."]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class TransferCustomer(Action):
    def name(self) -> Text:
        return "action_transfer_customer"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["You need to follow below steps for transferring customer:👇 1. Go to Customer Transfer Allow - 12228.Click new and fill up Source Branch, Destination Branch, Effect Till Date then save it. Follow the link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Information/CustomerTransferAllow/CustomerTransferAllow.aspx' target='_blank'>Customer Transfer</a> 2. Go to Customer Transfer Maintenance-11905.Click new and fill up required information then save it. Follow the link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Microfinance/CustomerTransfer/CustomerTransferMaintenance.aspx' target='_blank'>Customer Transfer Maintenance</a> 3. Go to Customer Transfer Acknowledge-12141.Click new and fill up branch, customer there save it. Follow the link <a href = 'http://139.5.71.109:8090/BANKING/Parameter/Microfinance/CustomerTransferAck/CustomerTransferAcknowledge.aspx' target='_blank'>Customer Transfer Acknowledge</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

class UploadSignature(Action):
    def name(self) -> Text:
        return "action_upload_signature"

    def run(self,dispatcher:CollectingDispatcher,tracker:Tracker,domain:Dict[Text,Any,])->List[Dict[Text,Any]]:
        reply = ["Please visit Signature Maintenance-11027. Follow the given link <a href = 'http://139.5.71.109:8090/BANKING/Deposit/Emp_SignatureMainTenance.aspx' target='_blank'>Signature Maintenance</a>"]
        
        user_id = tracker.sender_id
        query = tracker.latest_message['text']
        intent = tracker.latest_message['intent']
        entities = tracker.latest_message['entities']
        message = random.choice(reply)
        try:
            insert_logs(user_id,query,message,intent=intent,entities=entities)
            print ('inserted')
        except Exception as error:
            print ('Caught this error: ' + repr(error))
            pass
        dispatcher.utter_message(message)
        return []

