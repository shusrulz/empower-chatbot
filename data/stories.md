## happy path
* greeting
    - action_greeting
    - action_restart

## say goodbye
* goodbye
    - action_goodbye
    - action_restart

## interactive_story_1
* not_displaying_page_form
    - action_not_displaying_page_form
    - action_restart

## interactive_story_2
* menu_unavailable_after_logging
    - action_menu_unavailable_after_logging
    - action_restart

## interactive_story_3
* day_end_too_long_to_complete
    - action_day_end_too_long_to_complete
    - action_restart

## interactive_story_4
* problem_with_customer_id
    - action_problem_with_customer_id
    - action_restart

## interactive_story_5
* not_generating_loan_or_deposit_schedule
    - action_not_generating_loan_or_deposit_schedule
    - action_restart

## interactive_story_9
* trail_balance_mismatch
    - action_trail_balance_mismatch
    - action_restart

## interactive_story_10
* branch_enrollment_of_employee
    - action_branch_enrollment_of_employee
    - action_restart

## interactive_story_11
* about_INIFINITY_bot
    - action_about_INIFINITY_bot
    - action_restart

## interactive_story_12
* profane_words
    - action_profane_words
    - action_restart

## interactive_story_2
* change_interest_rate{"account_type": "deposit"}
    - slot{"account_type": "deposit"}
    - fetchtypeofaccount
    - slot{"account_type": "deposit"}
    - action_restart

## interactive_story_4
* change_interest_rate{"account_type": "loan"}
    - slot{"account_type": "loan"}
    - fetchtypeofaccount
    - slot{"account_type": "loan"}
    - action_restart

## interactive_story_1
* create_bank_account
    - action_create_bank_account
    - action_restart

## interactive_story_2
* add_rights
    - action_add_rights
    - action_restart

## interactive_story_3
* create_employee_account
    - action_create_employee_account
    - action_restart

## story
* set_holiday
    - action_set_holiday
    - action_restart

## story
* create_general_ledger
    - action_create_general_ledger
    - action_restart

## story
* set_message_about_customer
    - action_set_message_about_customer
    - action_restart

## story
* find_user_list
    - action_find_user_list
    - action_restart

## story
* set_teller_right
    - action_set_teller_right
    - action_restart

## story
* provide_salary
    - action_provide_salary
    - action_restart

## story
* see_voucher_details
    - action_see_voucher_details
    - action_restart

## story
* gl_statement
    - action_gl_statmenet
    - action_restart

## story
* check_daily_transaction
    - action_check_daily_transaction
    - action_restart

## story
* denomination_vault
    - action_denomination_vault
    - action_restart

## story
* teller_balance_status
    - action_teller_balance_status
    - action_restart

## story
* see_brach_from_head_office
    - action_see_brach_from_head_office
    - action_restart

## story
* see_sub_ledger_balance_and_transaction
    - action_see_sub_ledger_balance_and_transaction
    - action_restart

## story
* see_staff_advance
    - action_see_staff_advance
    - action_restart

## story
* transfer_amount_between_accounts
    - action_transfer_amount_between_accounts
    - action_restart

## story
* installment_schedule_none_recurring_product
    - action_installment_schedule_none_recurring_product
    - action_restart

## story
* change_customer_holder
    - action_change_customer_holder
    - action_restart

## story
* release_account_dormant_status
    - action_release_account_dormant_status
    - action_restart

## story
* transfer_cooperative_balance
    - action_transfer_cooperative_balance
    - action_restart

## story
* inter_branch_transaction
    - action_inter_branch_transaction
    - action_restart

## story
* about_schedule_loan_account
    - action_about_schedule_loan_account
    - action_restart

## story
* about_zero_principal
    - action_about_zero_principal
    - action_restart

## story
* about_overdraft_loan
    - action_about_overdraft_loan
    - action_restart

## story
* print_trial_balance
    - action_print_trial_balance
    - action_restart

## story
* about_loan_schedule_calculator
    - action_about_loan_schedule_calculator
    - action_restart

## story
* create_new_customer
    - action_create_new_customer
    - action_restart

## story
* about_obligator_customer_information
    - action_about_obligator_customer_information
    - action_restart

## story
* fill_non_residential_information
    - action_fill_non_residential_information
    - action_restart

## story
* fill_other_bank_information
    - action_fill_other_bank_information
    - action_restart

## story
* mandotry_field_kyc
    - action_mandotry_field_kyc
    - action_restart

## story
* setup_dormant
    - action_setup_dormant
    - action_restart

## story
* transfer_customer
    - action_transfer_customer
    - action_restart

## story
* nepali_information_in_kyc
    - action_nepali_information_in_kyc
    - action_restart

## story
* show_customer_transfer_report
    - action_show_customer_transfer_report
    - action_restart

## story
* show_kyc_copomis_report
    - action_show_kyc_copomis_report
    - action_restart

## story
* revert_loan_payment_voucher
    - action_revert_loan_payment_voucher
    - action_restart

## story
* cheque_status_report
    - action_cheque_status_report
    - action_restart

## story
* loan_disbursement_reverse
    - action_loan_disbursement_reverse
    - action_restart

## story
* renew_loan
    - action_renew_loan
    - action_restart

## story
* upload_signature
    - action_upload_signature
    - action_restart

## story
* deposit_renew
    - action_deposit_renew
    - action_restart

## story
* reverse_deposit_account
    - action_reverse_deposit_account
    - action_restart

## story
* find_customer_information
    - action_find_customer_information
    - action_restart

## story
* expired_loan_report
    - action_expired_loan_report
    - action_restart

## story
* customer_loan_balance
    - action_customer_loan_balance
    - action_restart

## story
* create_collector
    - action_create_collector
    - action_restart

## story
* set_rights_to_user
    - action_set_rights_to_user
    - action_restart

## story
* set_teller_right_in_branch
    - action_set_teller_right_in_branch
    - action_restart

## story
* check_user_group_right
    - action_check_user_group_right
    - action_restart

## story
* set_limit_on_user
    - action_set_limit_on_user
    - action_restart

## story
* employee_display_salary_sheet
    - action_employee_display_salary_sheet
    - action_restart

## story
* tag_employee_salary_to_salary_sheet
    - action_tag_employee_salary_to_salary_sheet
    - action_restart

## story
* decrease_loan_limit
    - action_decrease_loan_limit
    - action_restart

## story
* change_interest_amount
    - action_change_interest_amount
    - action_restart

## story
* change_recurring_installment_amount
    - action_change_recurring_installment_amount
    - action_restart

## interactive_story_1
* change_interest_rate{"account_type": "loan"}
    - slot{"account_type": "loan"}
    - fetchtypeofaccount
    - slot{"account_type": "loan"}
    - action_restart

## interactive_story_2
* change_interest_rate{"account_type": "deposit"}
    - slot{"account_type": "deposit"}
    - fetchtypeofaccount
    - slot{"account_type": "deposit"}
    - action_restart

## interactive_story_1
* change_interest_rate{"account_type": "Loan"}
    - slot{"account_type": "Loan"}
    - fetchtypeofaccount
    - slot{"account_type": "Loan"}
    - action_restart

## interactive_story_2
* change_interest_rate{"account_type": "Deposit"}
    - slot{"account_type": "Deposit"}
    - fetchtypeofaccount
    - slot{"account_type": "Deposit"}
    - action_restart

## interactive_story_3
* change_interest_rate
    - actionshowtypeofaccount
* change_interest_rate{"account_type": "loan"}
    - slot{"account_type": "loan"}
    - fetchtypeofaccount
    - slot{"account_type": "loan"}
    - action_restart

## interactive_story_4
* change_interest_rate
    - actionshowtypeofaccount
* reverse_deposit_account{"account_type": "deposit"}
    - slot{"account_type": "deposit"}
    - fetchtypeofaccount
    - slot{"account_type": "deposit"}
    - action_restart
## interactive_story_1
* open_multiple_accounts
    - action_open_multiple_accounts
    - action_restart

## interactive_story_1
* out_of_scope
    - action_out_of_scope
    - action_restart

## interactive_story_1
* after_greet
    - action_after_greet
    - action_restart

## interactive_story_1
* talk_to_person
    - action_talk_to_person
    - action_restart

# store_chat_logs_story
* extract_chat_logs
    - actionstorelogs
    - action_restart

# store_chat_logs_storyy
* extract_chat_logs
    - actionstorelogs
    - action_restart

# store_chat_logs_storyyy
* extract_chat_logs
    - actionstorelogs
    - action_restart
