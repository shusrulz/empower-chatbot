## intent: extract_chat_logs
- I want to get the conversation logs
- conversation logs.
- Give me the chat logs.
- store the chat logs.
- Where are the chat logs?
- I want the conversation history.
- Please give me the conversation logs.
- Where are the chat logs stored?
- chat logs.

## intent:about_INIFINITY_bot
- What is this chatbot?
- chatbot
- chatbot
- about chatbot
- What can you do?
- What services do you provide?
- What are you capable of?
- How can you help me?
- Tell me about yourself
- What do you do
- Tell me about your services
- Why are you built?
- What is your purpose
- I want to know about you
- Timi k ho?
- Timro kaam k ho
- Timi lay k garna sakxau
- Timi lai kina banaako
- Timro baare bhana ta
- Yo k ho
- What is this?
- Who made this chatbot?
- What can this chatbot do?
- How can this chatbot help?
- How can this chatbot be helpful?-
- yo chatbot le k garxa ta?
- yo chatbot k ho?
- yo chatbot le k k garna sakxa ta
- yo chatbot k ko lagi ho
- yo chatbot nai ho ta/
- yo chatbot ho ki manxe ho?
- kasle reply gardai xa?
- huh reply kasari aayo?
- khatra chatbot ho ta yo?
- yo system k ho

## intent:about_loan_schedule_calculator
- What is loan schedule calculator used for
- Tell me about loan schedule calculator
- What is loan schedule calculator
- How to use loan schedule calculator
- Detail on loan schedule calculator
- Loan schedule calculator vaneko k ho

## intent:about_obligator_customer_information
- What is obligator in customer information maintenance
- Tell me about obligator in customer information maintenance
- I want to know about obligator
- About obligator customer information
- Obligator customer information vaneko k ho

## intent:about_overdraft_loan
- Tell me about overdraft loan
- What is overdraft loan
- Describe overdraft loan
- Overdraft loan vaneko k ho
- Malai overdraft loan ko barema information chahiyo
- Can i know about the overdraft loan

## intent:about_schedule_loan_account
- Is the schedule of a loan account created while opening it or at its disbursal and why
- How is the schedule of the loan account created
- Process to create schedule loan account
- Schedule loan account kasari kholne

## intent:about_zero_principal
- What is zero principal loan settlement screen used for
- Tell me about zero principal loan settlement
- What is the purpose of zero principal

## intent:add_rights
- Add privileges to user
- Provide user with privilege
- Give user new rights
- User group ko rights kasari maintain garne
- Manage rights
- Add rights
- Rights
- User rights
- User Group Menu Rights
- Add privileges
- Provide privilege to user
- User lai rights dina paryo
- Maintain user group menu rights
- Manage user group menu rights
- add more rights to user

## intent:after_greet
- how are you?
- how are you doing
- are you fine?
- are you ok
- how is your condition?
- how have you been holding?
- kasto xa?
- what's up
- Hey, what is up?
- Hey, what' up
- Hey. what's up?
- hey. how are you.
- hi. how are you
- hi. what's up
- hi how are you
- how are you
- how are you
- how are you?
- how are you doing
- are you fine?
- are you ok
- how is your condition?
- how have you been holding?
- kasto xa?
- what's up
- Hey, what is up?
- Hey, what' up
- Hey. what's up?
- hey. how are you.
- hi. how are you
- hi. what's up
- hi how are you
- how are you
- how are you
- K xa bro
- K xa hero
- k xa boro
- K xa
- Sanchai
- Sanchai xau
- Aramai
- Araamai xau bro
- K xa solti
- how are you

## intent:branch_enrollment_of_employee
- I want to find out the branch of a certain employee
- Show the process of finding the branch of an employee
- I want to know the branch in which employee is.
- Employee kun branch ma xa hernu paryo.
- How do I know the current branch enrollment of an employee?
- How can I know the branch enrollment of an employee?
- Kun employee kun branch ma xa kasare taha painxa
- Kun employee kun branch ma xa hernu paryo
- Kun chai staff kun chai branch ma xa
- Staff ko branch hernu paryo
- Staff ko branch kasare herney
- Staff kun branch ma xa
- Staff kun chai branch ma kaam gardai xa
- I want to see in which branch an employee has been assigned
- Can you tell me how to find out the branch of an employee
- Employee kun branch ma xa?
- Employee vayeko branch kasari herne ho?
- How to know which employee is in which branch/
- In which branch is this employee working?
- Yo employee kun branch ma kaam garirako xa?
- In which branch is this employee enrolled?
- Employee branch enrollment
- Hey there can you tell me how to see the branch of the staff
- What is the process to check the branch in which a staff is working
- How can i find the branch of an employee
- Help me in finding the branch of the employee
- Want to know the branch of a staff
- Kun manxe kun branch ma xa kasare herney
- Manxe ko brach patta laaunu paryo
- Staff ko branch kasare herney
- Staff kun branch ma xa kasare herney-
- employee kun branch ma xa kasare herney ho

## intent:change_customer_holder
- How to change customer holder of a deposit account
- Change the customer account
- Change the account of the customer
- Customer ko account change garnu paryo
- Customer ko account kasari change garxa
- Customer ko account change garne k process cha

## intent:change_interest_amount
- How to increase the amount of interest to the loan account
- process to decrease the amount of interest to the loan account
- Tell me the step to increase the amount of interest to the deposit account
- How to decrease the amount of interest to the deposit account
- Change interest amount
- Interest amount change garna paryo
- Interest amount

## intent:change_interest_rate
- [deposit](account_type) account ko interest change garnu parne xa
- [loan](account_type) ko interest kasare change garney
- Interest rate change garnu parne
- Interest rate kasare change garney ho
- How to change the interest rate change
- [deposit](account_type) ko interest change garna k garnu parxa
- [loan](account_type) ko interest rate kasare change garne ho
- Show the process of ca
- [deposit](account_type) account ko interest kasare change garney
- Malai [deposit](account_type) account ko interest change garna sikauanu paryo
- Malai [loan](account_type) account ko interest rate change garna aayena
- [loan](account_type) ko interest rate change garne kasari ho
- [deposit](account_type) ko interest rate change garne kasari ho
- I need help in changing interest rate for [deposit](account_type)
- I need help in changing interest rate for [loan](account_type)
- [loan](account_type) ko interest rate change garnu paryo
- [deposit](account_type) ko interest rate change garnu paryo
- How to change interest rate for [loan](account_type).
- Change interest rate for [deposit](account_type).
- I want to change the [loan](account_type) interest rate.
- I want to change the interest rate for [deposit](account_type).
- Malai mero [deposit](account_type) account ko interest rate change garnu parne.
- [loan](account_type) ko interest rate change garna k garnu parxa?
- How to change the interest rate?
- Interest rate change garna k garnu parxa?
- Interest rate k garera change garna sakinxa?
- [deposit](account_type) Interest Rate Change Management
- [loan](account_type) Interest Rate Change Management
- How can I change the interest rate for [loan](account_type)?
- How should I change the interest rate for [deposit](account_type)?-
- [loan](account_type) ko interest rate change kasari garxa
- interest ko rate kasare change garne ho
- [deposit](account_type) ko interest rate change garne tarika k ho
- i want to change interest rate
- i want to change interest rate for [deposit](account_type)
- i want to change interest rate
- i want to change interest rate for [loan](account_type)
- malai interest rate change garnu paryo
- [loan](account_type)
- malai [loan](account_type) ko interest rate change garnu parne thyo
- ani deposit ko lagi chai k garne ?
- malai [loan](account_type) ko interest rate garnu xa
- ani [deposit](account_type) ko lai chai k garne
- i want to change the interest rate for [deposit](account_type)
- and how about [loan](account_type)
- malai interest ko rate change garnu xa
- [loan](account_type)
- now show the process for changing in [deposit](account_type)
- how to change rate in interset
- and now can you tell for the [deposit](account_type) side
- [deposit](account_type) ko rate kasare chagne garney
- ani [loan](account_type) ko chai?
- malai [loan](account_type) ko rate change garauna sekaau
- ani [deposit](account_type) ko lai chai kasare garne ho feri
- mero ma interest ko rate change garna aayena
- [loan](account_type)
- ani [deposit](account_type) ko ma chai
- change interest rate of [Loan](account_type)
- change interest rate for the [Deposit](account_type) account
- i want the [loan](account_type) interest rate to be changed
- change interest rate of [Deposit](account_type)
- change interest rate of [loan](account_type)
- change [Deposit](account_type) interest rate
- change rate of interest
- [loan](account_type)
- change interest rate please

## intent:change_recurring_installment_amount
- How can i change the recurring installment amount
- How to change the amount in recurring installment
- Change recurring installment amount
- recurring installment amount
- installment amount
- recurring installment amount change garna paryo
- recurring installment amount change garne kasari ho??

## intent:check_daily_transaction
- How to check daily transaction
- Check daily transaction
- Process for checking daily transaction
- Procedure to look the daily transaction details
- From where to look daily transaction

## intent:check_user_group_right
- How can i check the rights of a user group
- How to view the rights of a user group
- What is the process to check a user group rights
- I want to check the rights of the user
- Procedure of checking rights of the user
- Check user group right
- See user group rights
- View user group rights
- User group rights

## intent:cheque_status_report
- How to check the status report of the cheque
- What is the process to check the cheque status report
- I want to look at the report of the cheque

## intent:create_bank_account
- I want to create a bank account
- How to create bank account
- Bank account kasare banaaune
- Bank account banau
- Let me create a bank account.
- Create bank account
- Bank account
- Open bank account
- I need to open a bank account
- Bank account kholne kasari ho?
- Bank account kasari banaune
- 1ta bank account banaunu paryo
- From where can I create a bank account?
- Bank account kata bata banaune ho?
- create bank account

## intent:create_collector
- How to create collector
- What is the process to create collector
- I want to create collector
- Malai collector banaunu paryo
- Collector kasari banaune ho
- Create a collector
- Make a new collector
- Naya collector banaunu paryo
- Create collector
- Colector banaunu paryo

## intent:create_employee_account
- How to create employee account
- What is the rule to open an account for employee
- Process for creating account for staff
- How to know if we can create an employee account or not
- In which product we can create employee a/c
- Employee account
- Employee account banaunu paryo
- Employee account banauna paina?
- create account for employee

## intent:create_general_ledger
- How to create general ledger
- General ledger kasari banaune
- Help to make a ledger
- Ledger banauna help chahiyo
- What is the process to make a general ledger
- General ledger banaune process k ho
- Ledger kasare banaauxa
- Steps to create general ledger
- General ledger banaune step k k ho
- Ledger
- General ledger
- Can we create a general ledger
- Ledger kasari banaune??

## intent:create_new_customer
- How to create new customer
- Create new customer
- Make new customer account
- Help to make a new account for the customer
- I want to create new customer
- Naya customer banaunu paryo
- Naya customer kasari banaune ho

## intent:customer_loan_balance
- How can i find customer loan balance
- About customer loan balance
- Customer ko loan balance kasari paune
- Customer ko loan balance ka huncha
- Know about customer loan balance
- Get all the loan balance of the customer
- I want to view the loan balance of the customer
- Find the total balance of the loan of the customer
- View customer loan balance
- Customer loan balance
- Loan balance of customer

## intent:day_end_too_long_to_complete
- Dayend nai bhayena
- Dayend ma error aayo
- Dayend garda kheri error dekhaauxa
- Day end hudaina mero ma
- Mero ma day end garda kaam nai garena
- Mailay day end garna khojeko kaam garena
- Day end garnu khojda mistake dekhayo
- Day end garda mistake bhanxa
- Day end ma problem hunxa
- Dayend garna khojda problem aauxa
- Dayend lay fail khanxa
- day end kina na bhako mero ma
- Day end kina hudaina
- Mero ma dayend ko kaam nai hunna
- Mero ma day end ko kaam
- There is problem with dayend.
- Dayend dekhayena.
- There is an error with dayend.
- I am getting errors with dayend.
- Dayend ma error dekhayo.
- Error dayend.
- Why is there a delay in dayend
- Too much time taking in dayend
- Dayend is taking too much time
- Dayend ma time lagiracha mero
- Dayend ma kina yeti dherai time lagiracha
- Problem with dayend.
- Mero dayend ma problem aairacha
- There is some problem with dayend
- What is the problem with dayend?
- How am I getting error on dayend.
- I am not able to complete my transactions with dayend.
- My dayend transaction is not complete.
- How to complete the dayend?
- What should we do to complete dayend?
- How dayend can be completed?
- Dayend rokdinu ta vayenani
- Mero dayend vayena k garni hola
- Why is there problem with dayend?-
- mero dayend garda error aauxa
- which day is today

## intent:decrease_loan_limit
- How can i decrease loan limit
- I want help in decreasing loan limit
- Loan limit kasari ghataune
- Decrease loan limit
- Procedure for decreasing loan limit
- What is the process to decrease the limit amount for loan
- Loan limit
- Change loan limit
- Increase loan limit
- Alter loan limit

## intent:denomination_vault
- From where can i be able to find denomination of vault
- Problem in finding denomination of vault
- Search denomination vault
- Cannot find denomination vault

## intent:deposit_renew
- Can we renew deposit in the same account number
- Renew deposit under same a/c number
- How to renew deposit
- Process to renew the deposit
- How can we change the renew of deposit
- Change the deposit of renew

## intent:employee_display_salary_sheet
- How can i setup employee to display in salary sheet
- How can employee be displayed in salary sheet
- Employee lai salary sheet ma dekhauna paryo
- Help me in displaying employee in salary sheet
- I want to display employee in salary sheet
- Display employee salary sheet
- Salary sheet of employee

## intent:expired_loan_report
- From where can i view expired loan report
- View the report of the loan which are expired
- How to see the information of the loans which are expired
- Get the loan information which are expired
- Expired loan information view
- Expired loan report
- Loan report expiry

## intent:fill_non_residential_information
- How to fill up non residential information
- Fill non residential information
- Process for filling non residential information
- I want to fill non residential information
- How to fill non residential information
- Procedure to fill non residential information
- Non residential information kasari fill garne ho
- Malai non residential information fill garnu paryo

## intent:fill_other_bank_information
- How to fill ip dealing other bank information
- How to fill information of other bank
- Process to fill the information of bank when we have to deal with them
- Tell me how to fill details of other bank
- I want to fill other bank information
- Aru bank ko information kasari varne ho
- Help me on filling other bank information

## intent:find_customer_information
- How can i be able to find whole information of a customer
- How to see the details of a customer
- From where can i view customer information
- View customer detail
- Customer detail herna paryo
- I want to see the information of customer
- Let me find some information of customer.

## intent:find_user_list
- From where i can find all user list?
- Find user list
- User haru ko list kasare herney
- How to see all users
- Help to find list of the users

## intent:gl_statement
- Is it possible to see gl statement report
- Can i look gl statement report
- How can i look gl statement

## intent:goodbye
- Goodbye
- Yes, thank you for helping me out.
- Thank you for helping me out
- Byebye
- Thank you for guiding me
- thank you
- Thanks for helping me out
- Goodbye
- Thanks for helping me out
- I am done.
- Thank you
- Bye
- Dhanyabaad
- Sahaoyog ko lagi dhanyabaad.
- This was helpful.
- This chatbot really helped. Thanks.
- Sahaoyog ko lagi dhanyabbad
- Ma gaye hai
- Aba jaanxu hai
- Ta ta
- tata
- ok
- Ok

## intent:greeting
- Hello
- Good morning
- hello
- hi
- hello
- Hey
- Good evening
- Hello bot
- Hi infinity
- Hello
- Hello there infinity
- Namaste
- Namskaar
- namaskaar
- namaskar
- namaste
- ani k xa ta khabar?
- Pranipat chatbot shree

## intent:installment_schedule_none_recurring_product
- Is it possible to schedule installment in none recurring product
- How to schedule installment in none recurring product

## intent:inter_branch_transaction
- What is inter branch transaction
- Tell me about inter branch transaction
- About inter branch transaction

## intent:loan_disbursement_reverse
- Is loan disbursement able to reverse
- Can we reverse the loan disbursement
- What is the process to reverse the disbursed loan
- Reverse the loan that is already disbursed

## intent:mandotry_field_kyc
- How to setup mandatory different field in kyc
- How to setup mandatory field in customer information
- What is the process to have compulsory field when filling the customer information
- Way to setup mandatory field in customer information
- Kyc ko lagi mandatory field kasari setup garne

## intent:menu_unavailable_after_logging
- Menu is not available
- Menu is not showing
- Menu is not shown even after logging in
- No menu after logging
- There is no menu after logging
- I can't see the menu
- Menu is not showing up
- Menu nai dekhayena
- Why is the menu not showing
- Problem in displaying menu
- Kei pani dekhayena
- System ma kei pani dekhayena
- No menu after logging in
- There is no menu after logging
- System ma kei ne dekhayena
- Mero ma menu dekhiyana
- Menu nai aayena
- Mero ma login garepachi menu kina aayena
- login nai hudaina mero
- Mero login kina na bhaako
- System ma kei ni ako chaina
- System khali aauxa
- System khali dekhauxa
- Login bhayepaxi system ma kei aayena
- Login bhayepaxi system lay kei dekhaayena
- Khai ta menu nai aaudaina ta login ko-
- login gare pani system ma kei pani dekhena

## intent:nepali_information_in_kyc
- How to fill up nepali information in kyc
- Fill nepali information in kyc
- Kyc ma nepali information fill kasari garne
- Help me in filling nepali information in kyc
- I want to fill nepali information in kyc. How to do it.

## intent:not_displaying_page_form
- after updating the page form is not displaying correctly
- I have a problem in loading page form
- Why is the page form not coming
- Why is the page form not coming even its updated correctly
- How to solve the problem of page form not loading
- Problem in loading page form
- Page form not displaying properly
- The form is not loading
- The pages are not loading properly
- The webpage is not loading
- My webpage is not displaying
- Error in loading the webpage
- My page form is not displaying properly.
- Page form is not loading.
- After I update, page form is not displaying.
- My page form is not displaying
- I have updated but i can't see the page form
- Problem on page form
- How to display the page form?
- Page form is not loading , what should i do?.
- Error in page form loading
- Screen lay kaam garena k garne ho
- Screen nai aaudaina
- Mero screen ma kei ne aayena
- Mero screen khaale aauxa
- Mero screen ma kina kei na aako
- Screen lay kaam garena
- Screen ma kei aaaudaina ta
- Page ma kei dekhayena
- Page khali aauxa mero ma
- Sceen hang vayo mero
- Update gare pachi pani screen nai dekhayena
- Mero screen chalirachaina
- Sir mero screen le kaam gardaina
- Computer ma kei ayena
- Computer ma kei dekhayena
- Kei dekhaudaina ta yaar-
- mero screen nai display hunna
- page ta khaale xa ta
- page khaale xa

## intent:not_generating_loan_or_deposit_schedule
- loan schedule generate garna milena
- deposit schedule generate garna milena
- Schedule nai generate hudaina ta
- loan ko schedule nai generate bhayena
- deposit ko schedule nai generate bhayena
- Schedule nekaalna milena
- Schedule nai aaudaina
- loan schedule nai aayena
- deposit schedule nai ayena
- loan ko schedule nai ayena
- deposit ko schedule nai ayena
- Unable to generate loan/ deposit schedule for microfinance account.
- Disbursement maa schedule dekhayena
- Schedule generate vayena
- I cannot generate loan schedule for microfinance account.
- Schedule not generated
- Error with schedule generation
- deposit pachi schedule nai aaena k garne
- Unable to get loan schedule for microfinance
- There is problem with generating deposit schedule for microfinance account.
- Problem with loan schedule generation.
- Problem with deposit schedule generation.
- I am unable to get the loan schedule for microfinance account.
- Cannot get the deposit schedule for microfinance account.
- loan schedule aayena
- deposit schedule nai aaudaina
- Khoi ta loan schedule?
- Khoi ta deposit schedule?
- Mero ma loan schedule dekhayena
- Mero ma deposit schedule dekhayena
- Problem with generating schedule.
- There is not schedule in disbursement.
- I cannot get the schedule in disbursement for loan.
- I cannot get the schedule in disbursement for deposit.
- Help me with getting schedule in disbursement for deposit
- Help me with getting schedule in disbursement for loan
- How to get the loan schedule in disbursement?
- How to get the deposit schedule in disbursement?-
- loan schedule generate huna

## intent:open_multiple_accounts
- Can i open multiple account
- Is there any way i can open multiple account
- Is opening multiple account possible
- I want to open multiple account
- Do you provide the facility to open multiple accounts
- can i open multiple account

## intent:out_of_scope
- the earth is round
- who is the prime minister of nepal
- haha hahah hahah hahahaha
- kya jhyauu yar
- i am hungry
- lets have lunch
- lets go to watch movie
- film herna jaam
- bholi bida xa ke nai
- what is the weather today
- lets play some game
- go wash dishes
- the earth is flat i can prove it
- dance for me
- sing a song for me
- who is the hero in Titanic
- who sang the song dilbar dilbar
- who will win the premier league
- david beckham is very handsome
- do you want to fight with me?
- fight khelne ho single singele aija
- i want to know the president of USA
- give me 1 billion dollar
- what is info developer
- my teeth is paining
- my mobile device is not working
- the screen of my television is not working
- i want to eat
- what is the size of earth
- earth is round
- bla bla bla
- fsdfsdfsd dfdfd
- vsjdfklsdj kjfsldkfj
- fjskdjfkldfj

## intent:print_trial_balance
- How can you print trial balance in infinity
- How to get trial balance in infinity
- I want to print trial balance
- Malai trial balance print garnu paryo
- Trial balance print kasari garne

## intent:problem_with_customer_id
- Staff account kholne bela ma naam dekhaayena
- Customer id search garna nai milena
- Employee search nai garna melena
- Id Search garna milena
- Customer lai search garna milena
- Staff search garna milena
- Customer lai kina ho search nai garna mildaina ta
- Customer lai search garnai mildaina
- Customer khojna milena
- Customer ko id search garna mildaina kina ho
- Customer search hudaina
- Staff search hudaina
- customer id search hudaina
- Staff account khole paxi search garna khojda search nai bhayena
- Staff account kholera search garna vayena
- Staff account kholera customer lai search garda
- Customer id cannot be searched while opening deposit or loan account of an employee.
- I cannot get the customer id.
- Customer ID cannot be obtained.
- Customer ID aayenani ta bro.
- I cannot see the name after opening staff account.
- Customer is not searched while opening staff account.
- Name is not displayed while opening staff account.
- No customer id
- Can't get customer id
- I can't find customer id
- Customer ID and Customer name is not obtained.
- There is a problem with customer ID.
- How to obtain customer ID and customer name?
- Customer ID dekhayena
- Not showing customer ID or customer name.-
- customer search garna melena

## intent:profane_words
- Fuck this chatbot.
- Fuck you.
- Esto mulla chatbot.
- Hawa jasto chatbot
- Ghanta jasto chatbot
- Khoya jasto chatbot
- This chatbot is shit.
- Useless fucking chatbot.
- Fuck
- Ghanta
- yo muji chatbot kei kaam xaina
- esto hawa chatbot banayera ta vayenani
- bhalu
- ta khatey hero bhako
- kutreya
- khopdi tod saaley ka

## intent:provide_salary
- How to provide salary through head office
- Provide salary from head office
- Give salary to employee from head office
- Head office bata salary kasare diney

## intent:release_account_dormant_status
- How can you release account with a dormant status
- How to release an account having dormant status
- Release dormant account

## intent:renew_loan
- Loan renew can able in same a/c number
- Can we renew loan in the same account number
- Is it possible to renew loan under the same a/c
- How to renew the loan
- Loan renew
- Loan renew garne kasari ho??
- How can I renew the loan??
- Let me renew the loan

## intent:reverse_deposit_account
- Can i reverse deposit account close?
- Reverse deposit account closing
- Deposit account closing
- Closing of deposit account
- Alter closing of deposit account
- Manage closing of deposit account
- How to revert closing of deposit account?
- Deposit account closing change
- Close vaisakeko deposit account kasari change garne
- Deposit vyaeko amount change garnu payo??
- [deposit](account_type)

## intent:revert_loan_payment_voucher
- How can i revert loan payment voucher
- What is the procedure to revert the loan payment
- Tell me how to revert loan payment voucher
- How can the loan payment voucher be reverted

## intent:see_brach_from_head_office
- Can we look at the bank account of the branch from the head office?
- Is it possible to look at branch account from the main office
- Is bank account of branch possible to saw from head office?
- how to find branch office from the head office
- find branch offices
- search branch office

## intent:see_staff_advance
- From where can i see staff advance amount employee wise
- How to look the advance amount paid to the staff
- View the staff to whom advance amount has been paid
- search the amount that is paid in advance to the employee
- search advance amount given to employee

## intent:see_sub_ledger_balance_and_transaction
- Can we look at the sub ledger balance and transaction report
- Is it possible to look a the sub ledger balance and transaction report
- how to look sub ledger balance
- how to look transaction report
- where is sub ledger balance
- where is transaction report
- search sub ledger and transaction report

## intent:see_voucher_details
- How to look voucher details
- From where can i see voucher details
- Voucher details kasare herney

## intent:set_holiday
- How to set holiday?
- Holiday set kasari garne
- Process to set a holiday
- Holiday set garne process k ho
- Bida kasare melaaune
- Add holiday
- Bida thapnuparyo
- Holiday
- bida
- Calender
- Calender mantainence
- Maintain holiday

## intent:set_limit_on_user
- How can i set limit to a user
- How to set a limit of an amount to a user group
- Set limit to user
- Set limit user
- Set limit for user
- User lai limit garna paryo

## intent:set_message_about_customer
- Can i set message about customer
- Set message about a customer
- Message customer
- Set message customer
- Customer lai message kasari set garne
- How to set customer message?

## intent:set_rights_to_user
- How to set rights to user
- What is the process to set rights to the user
- How can i give rights to a user
- User lai rights kasari dine
- I want to give rights to user
- Help me with setting rights to the user
- Malai user lai right set garnu paryo
- Provide rights to user
- Give rights user

## intent:set_teller_right
- How to set teller right for user
- assign a teller right to a user
- help to give a teller right to the user

## intent:set_teller_right_in_branch
- How can i assign teller right in branch 002
- What is the process to assign teller in branch
- Help me in assigning teller in a branch
- I want to assign teller right in branch
- Malai branch ma teller right set garnuparyo
- Branch ma teller right kasari set garne
- Set teller right in branch
- Teller right in branch

## intent:setup_dormant
- How to setup dormant while customer’s information is not available fully
- Create dormant account when we do not have customer information
- No customer information how to setup dormant
- Purai customer information navako ma dormant kasari setup garne
- Customer ko information chaina aba dormant kasari setup garne

## intent:show_customer_transfer_report
- How to look the customer transfer report
- What is the step to look to the customer transfer report
- I want to get the transfer report of the customer
- I want to see transfer report of customer
- Malai customer ko transfer report hernu paryo

## intent:show_kyc_copomis_report
- How to show kyc report
- How to look at the copomis report
- I want to look COPOMIS report. Help me with it.
- Where can i find kyc report
- Where can i find copomis report
- Kyc copomis report kasari dekhaune
- Kyc  copomis report dekhaunu paryo
- I want to show kyc copomis

## intent:tag_employee_salary_to_salary_sheet
- How to tag employee salary account to salary sheet
- Process to tag employee salary account with salary sheet
- Employee ko salary lai salary sheet ma tag kasari garne
- I want to tag employee salary to salary sheet
- Help me with tagging employee salary to salary sheet
- Salary lai employee ko salary sheet ma tag garnu paryo
- Tag employee salary to salary sheet

## intent:talk_to_person
- i want to talk to a real person
- i do not want to talk to you. get me a real one
- give me the contact number i want to contact to a human being
- i need to talk to a human
- get me a person
- i want to switch to a human
- i want to switch to a customer care
- get me customer care number
- put me through customer care
- give me customer service
- i want to talk to customer service
- get me a person

## intent:teller_balance_status
- Which screen show teller balance status report
- Where is teller balance status report
- How to look teller balance status report
- Cannot find report of teller balance

## intent:trail_balance_mismatch
- Trial balance maa equal aayena
- Trial balance nai milena
- Trial balance mismatch aayo
- Trial balance farak auxa
- Trial balance ma amount equal bhayena
- Trial balance equal xaina
- Trial balance ko report ma difference aayo
- Trial balance ma amount barabar xaina
- Debit ra credit ko balance same xaina
- Debit ra credit ko amount farak xa
- Debit ra credit ko amount ma mistake aayo
- Trial balance ma mistake aayo
- Debit ra credit ko balance match xaina
- The amount in debit and credit column is different
- Debit ra credit ko amount different xa
- Debit ra credit ko balance different xa
- Periodic report lay debit ra credit ko balance farak dekhayo
- Trial balance ko report equal xaina
- Periodic report is showing difference in total Dr and Cr balance
- Trial Balance ma difference aayo.
- Trial balance kina milena
- There is difference in debit and credit balance.
- Debit and Credit balance is not matched.
- The balance in debit and credit is not matched.
- Error in periodic report.
- Periodic report error.
- Difference in total debit and credit balance.
- Trial Balance milena.
- Debit and credit same vayena.
- Debit and credit balance ma error aayo.
- Difference in periodic report.
- Difference in trial balance.
- Why is there an error in trial balance?
- Why are there errors in debit and credit amount in trial balance?
- The amount in debit and credit side is different
- The amount in debit and credit is not matching.-
- trial balance ma aako amount balance xaina

## intent:transfer_amount_between_accounts
- Can i send amount between the accounts
- How to transfer amount
- How to transfer amount from one account to another account
- transfer amount between accounts
- transfer money
- i want to transfer money between accounts
- how to transfer money

## intent:transfer_cooperative_balance
- Can i transfer the cooperative’s bank balance to cooperative vault transaction maintenance
- How to transfer balance of cooperative bank in the cooperative vault

## intent:transfer_customer
- How to transfer customer from one branch to another branch
- Help required to transfer customer
- Process to transfer customer
- I want to transfer customer
- Customer transfer garnu paryo
- Customer transfer kasari garne
- I have to transfer customer

## intent:upload_signature
- How to upload signature?
- Signature kasari upload garne??
- Upload signature
- Let me upload signature
- Signature ko abrema thaha xa boro
- Signature k ho?
- Signature nai mildaina boro
